"""Module of constatns for aws."""

import os

AWS_REGION = os.environ.get('REGION_AWS', 'us-east-1')
AWS_DYNAMODB_TABLE = os.environ.get('AWS_DYNAMODB_TABLE')
OK = 200
FAILED = 400
COGNITO_USER_POOL_CLIENT = os.environ.get('COGNITO_USER_POOL_CLIENT')
