"""Module for create clients."""

import json

import constants

from ulid import ULID

from orm_model import clients_model
from clients.manage_cognito import sign_up
from responses_status.helper import make_response


def lambda_handler(events, context):
    """Lambda executes the controller method. If the controller exists or returns a response, it becomes available to handle another event.

    Args:
        events: dict, required
            Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format
        context: bject, required
            Lambda Context runtime methods and attributes
            Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns:
        dict: API Gateway Lambda Proxy Output Format
        doc https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """
    body = json.loads(events.get('body'))
    if body.get('name') and body.get('email') and body.get('passwd'):
        name = body.get('name')
        email = body.get('email')
        passwd = body.get('passwd')
        identitier = str(ULID())
        cclient = sign_up(email=email, passwd=passwd)
        if cclient is dict:
            client = clients_model.create(identifier=identitier, name=name, email=email)
            if client is dict:
                return make_response(
                    status_code=constants.OK,
                    id_request=context.aws_request_id,
                    message='Client created successfully',
                    item=client,
                )
            error = client
        else:
            error = cclient
    return make_response(
        status_code=constants.FAILED,
        id_request=context.aws_request_id,
        message='Error when creating the client',
        error='{0}'.format(error),
    )
